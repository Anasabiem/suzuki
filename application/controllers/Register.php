<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('suzuki_model');
    }

	public function index()
	{
		// $data['daftar']=$this->suzuki_model->insert();
		$this->load->view('client/register');
	}
	public function buat(){
		if(isset($_POST['btnSimpan'])){
          $config = array('upload_path' => './gallery/gambar_user/',
                  'allowed_types' => 'gif|jpg|png|jpeg'
                  );
          $this -> load -> library ('upload',$config);
          if ($this->upload->do_upload('gambar_user'))
        {
            $upload_data = $this -> upload -> data ();
            $foto = "gallery/gambar_user/".$upload_data['file_name'];
            $nama = $this -> input -> post ('nama');
        	$username = $this -> input -> post ('username');
        	$pass = $this -> input -> post ('pass');
        $data = array(
        'profile' => $foto,
        'nama_admin'=>$nama,
        'username'=>$username,
        'password'=>$pass
        );
        $insert_data = $this->db->insert('admin',$data);
      }
      if ($insert_data) {
        header('location:'.base_url());
       } else{
        echo "string";
       }
    }else{
      echo "gagal";
    }
  }	
}