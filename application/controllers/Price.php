<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price extends CI_Controller {

	public function index()
	{
		$data['navbar']='client/side/navbarHome';
		$data['footer']='client/side/footer';
		$idertiga='1';
		$data['newertiga']=$this->suzuki_model->pricelistClient('10');
		$data['ignis']=$this->suzuki_model->pricelistClient('11');
		$data['ignisse']=$this->suzuki_model->pricelistClient('12');
		$data['baleno']=$this->suzuki_model->pricelistClient('13');
		$data['newx']=$this->suzuki_model->pricelistClient('14');
		$data['karimun']=$this->suzuki_model->pricelistClient('15');
		$data['vitara']=$this->suzuki_model->pricelistClient('16');
		$data['apv']=$this->suzuki_model->pricelistClient('17');
		$data['carry']=$this->suzuki_model->pricelistClient('18');
		$data['gambarErtiga']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 10));
		$data['gambarIgnis']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 11));
		$data['gambarIgnisSport']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 12));
		$data['gambarBaleno']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 13));
		$data['gambarSCross']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 14));
		$data['gambarKarimun']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 15));
		$data['gambarGrandVit']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 16));
		$data['gambarAPV']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 17));
		$data['gambarCarry']=$this->suzuki_model->selectwhere('mobil', array('id_model' => 18));
		$this->load->view('client/pricelist', $data);
	}
}
