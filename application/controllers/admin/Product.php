<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	function __construct(){

		parent :: __construct();
		$this->load->model('suzuki_model');

	}

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('Home');
		}else {
			$data['navbar']='admin/side/navbar';
			$data['sidebar']='admin/side/sidebar';
			$data['mobil'] = $this->suzuki_model->get_mobil();
			$this->load->view('admin/product', $data);
			// $this->load->view('admin/side/nav_product');
		}
	}

	public function tambahData(){
		$data['navbar']='admin/side/navbar';
		$data['sidebar']='admin/side/sidebar';
		$data['model'] = $this->suzuki_model->get_model();
		$data['automobile'] = $this->suzuki_model->get_automobile();
		$this->load->view('admin/t-product', $data);

	}
	public function create (){
		$data['nama_mobil']=$this->input->post('nama_mobil');
		$data['id_automobile']=$this->input->post('id_automobile');
		$data['id_model']=$this->input->post('id_model');
		$data['spek_panjang']=$this->input->post('spesifikasi_panjang');
    $data['spek_lebar']=$this->input->post('spesifikasi_lebar');
		$data['spek_tinggi']=$this->input->post('spesifikasi_tinggi');
		$data['spek_tempat_duduk']=$this->input->post('spesifikasi_tempat_duduk');
		$data['spek_tanki']=$this->input->post('spesifikasi_tanki');
		$data['embedcode_video_mobil']=$this->input->post('url_video');
		$data['gambar_mobil']=$this->input->post('gambar_mobil');
		$data['gambar_exterior']=$this->input->post('gambar_exterior');
		$data['gambar_interior']=$this->input->post('gambar_interior');
		$data['gambar_performance']=$this->input->post('gambar_performance');
		$data['gambar_safety']=$this->input->post('gambar_safety');
    $this->suzuki_model->insert('mobil', $data);
    header('location:'.base_url().'admin/Product');
	}

	public function hapus($id){
		$where = array('id_mobil'=>$id);
		$hapus = $this -> suzuki_model -> delete($where,'mobil');
		if($hapus){
		header('location:'.base_url('index.php/admin/Product')); ?>
		<div class="alert bg-red">
               Lorem ipsum dolor sit amet, id fugit tollit pro, illud nostrud aliquando ad est, quo e sse dolorum id
         </div>

            <?php
		}else{
			header('location:'.base_url('index.php/admin/Product'));
			echo "gagal";
		}
	}

	public function editData(){
		$id=$this->uri->segment(4);
		$data['detail'] = $this->suzuki_model->editProduct($id);
		$data['modelu'] = $this->suzuki_model->get_model();
		$data['automobile'] = $this->suzuki_model->get_automobile();
		$data['error'] = ' ';
		$this->load->view('admin/d-product', $data);
	}

	public function prosesEdit(){
		// $id=$this->uri->segment(4);
		$id=$this->input->post('id_mobil');
		$data['nama_mobil']=$this->input->post('nama_mobil');
		$data['id_model']=$this->input->post('id_model');
		$data['id_automobile']=$this->input->post('id_automobile');
		$data['spek_panjang']=$this->input->post('spek_panjang');
    $data['spek_lebar']=$this->input->post('spek_lebar');
		$data['spek_tinggi']=$this->input->post('spek_tinggi');
		$data['spek_tempat_duduk']=$this->input->post('spek_tempat_duduk');
		$data['spek_tanki']=$this->input->post('spek_tanki');
		$data['embedcode_video_mobil']=$this->input->post('embedcode_video_mobil');
		$this->suzuki_model->update('mobil', $data, array('id_mobil' => $id));
		header('location:'.base_url('admin/Product/editData/'.$id));
	}

//update foto mobil
		public function prosesEditGambarMobil()
		{
                $config['upload_path']          = 'gallery/Struktur';
                $config['allowed_types']        = 'gif|jpg|png';

                $this->load->library('upload', $config);

                if ( $this->upload->do_upload('userfile'))
                {
												$data = $this->upload->data();
												$name_file=$data['file_name'];
												$update_data['gambar_mobil'] = $name_file;
												$id_mobil=$this->input->post('id_mobil');
												$this->suzuki_model->update('mobil', $update_data, array('id_mobil'=>$id_mobil));
                        header('location:'.base_url('admin/Product/editData/'.$id_mobil));
                }
        }

				public function prosesEditGambarExterior()
				{
		                $config['upload_path']          = 'gallery/gambar_product';
		                $config['allowed_types']        = 'gif|jpg|png';

		                $this->load->library('upload', $config);

		                if ( $this->upload->do_upload('userfile'))
		                {
														$data = $this->upload->data();
														$name_file=$data['file_name'];
														$update_data['gambar_exterior'] = $name_file;
														$id_mobil=$this->input->post('id_mobil');
														$this->suzuki_model->update('mobil', $update_data, array('id_mobil'=>$id_mobil));
		                        header('location:'.base_url('admin/Product/editData/'.$id_mobil));
		                }
		        }

						public function prosesEditGambarInterior()
						{
				                $config['upload_path']          = 'gallery/gambar_product';
				                $config['allowed_types']        = 'gif|jpg|png';

				                $this->load->library('upload', $config);

				                if ( $this->upload->do_upload('userfile'))
				                {
																$data = $this->upload->data();
																$name_file=$data['file_name'];
																$update_data['gambar_interior'] = $name_file;
																$id_mobil=$this->input->post('id_mobil');
																$this->suzuki_model->update('mobil', $update_data, array('id_mobil'=>$id_mobil));
				                        header('location:'.base_url('admin/Product/editData/'.$id_mobil));
				                }
				        }

								public function prosesEditGambarPerformance()
								{
						                $config['upload_path']          = 'gallery/gambar_product';
						                $config['allowed_types']        = 'gif|jpg|png';

						                $this->load->library('upload', $config);

						                if ( $this->upload->do_upload('userfile'))
						                {
																		$data = $this->upload->data();
																		$name_file=$data['file_name'];
																		$update_data['gambar_performance'] = $name_file;
																		$id_mobil=$this->input->post('id_mobil');
																		$this->suzuki_model->update('mobil', $update_data, array('id_mobil'=>$id_mobil));
						                        header('location:'.base_url('admin/Product/editData/'.$id_mobil));
						                }
						        }

										public function prosesEditGambarSafety()
										{
																$config['upload_path']          = 'gallery/gambar_product';
																$config['allowed_types']        = 'gif|jpg|png';

																$this->load->library('upload', $config);

																if ( $this->upload->do_upload('userfile'))
																{
																				$data = $this->upload->data();
																				$name_file=$data['file_name'];
																				$update_data['gambar_safety'] = $name_file;
																				$id_mobil=$this->input->post('id_mobil');
																				$this->suzuki_model->update('mobil', $update_data, array('id_mobil'=>$id_mobil));
																				header('location:'.base_url('admin/Product/editData/'.$id_mobil));
																}
												}

//end of update foto mobil

// tampilan edit gambar
		public function editGambarMobil(){
			$id=$this->uri->segment(4);
			$data['gambar_mobil'] = $this->suzuki_model->editProduct($id);
			$this->load->view('admin/editGambar', $data);
		}

		public function editGambarExterior(){
			$id=$this->uri->segment(4);
			$data['gambar_exterior'] = $this->suzuki_model->editProduct($id);
			$this->load->view('admin/editExterior', $data);
		}

		public function editGambarInterior(){
			$id=$this->uri->segment(4);
			$data['gambar_interior'] = $this->suzuki_model->editProduct($id);
			$this->load->view('admin/editInterior', $data);
		}

		public function editGambarPerformance(){
			$id=$this->uri->segment(4);
			$data['gambar_performance'] = $this->suzuki_model->editProduct($id);
			$this->load->view('admin/editPerformance', $data);
		}

		public function editGambarSafety(){
			$id=$this->uri->segment(4);
			$data['gambar_safety'] = $this->suzuki_model->editProduct($id);
			$this->load->view('admin/editSafety', $data);
		}

// tampilan edit gambar
}
