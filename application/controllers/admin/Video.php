<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('Home');
		}else {
			$data['navbar']='admin/side/navbar';
			$data['sidebar']='admin/side/sidebar';
			$data['embed'] = $this->suzuki_model->select('headline');
			$this->load->view('admin/video', $data);
			// $this->load->view('admin/side/nav_berita');
		}
	}

	public function prosesVideo(){
		$embed = array('embedcode' => $this->input->post('embed'));
		$this->suzuki_model->update('headline', $embed, array('id_headline'=>1));
		header('location:'.base_url().'admin/Video');
	}
}
