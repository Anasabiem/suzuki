<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spesifikasi extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('Home');
		}else {
			$data['navbar']='admin/side/navbar';
			$data['sidebar']='admin/side/sidebar';
			$this->load->view('admin/spesifikasi', $data);
			// $this->load->view('admin/side/nav_spek');
		}
	}
}
