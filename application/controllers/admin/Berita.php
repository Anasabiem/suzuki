<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('suzuki_model');
    }

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
      redirect('Home');
    }else{
		$data['berita']=$this->suzuki_model->get_news();
		$this->load->view('admin/berita',$data);
	}
}
	public function tambahData(){
		$this->load->view('admin/t-berita');
	}
	public function create(){
        if(isset($_POST['btnSimpan'])){
        	$config = array('upload_path' => './gallery/gambar_berita/',
        					'allowed_types' => 'gif|jpg|png|jpeg'
        					);
        	$this -> load -> library ('upload',$config);
        	if ($this->upload->do_upload('berita'))
    		{
        		$upload_data = $this -> upload -> data ();
        		$judul = $this -> input -> post ('judul');
        		$tanggal = $this -> input -> post ('tanggal');
        		$isi = $this -> input -> post ('isi');
        		$sumber = $this -> input -> post ('sumber');
        		$foto = "gallery/gambar_berita/".$upload_data['file_name'];
				$data = array(
				'judul_berita' => $judul,
				'isi_berita' => $isi,
				'tgl_berita' => $tanggal,
				'sumber' => $sumber,
				'gambar_berita' => $foto
				);
				$insert_data = $this->db->insert('berita',$data);
			}
			if ($insert_data) {
				redirect(base_url().'admin/Berita');
			 } else{
				echo "string";
			 }
		}else{
			echo "gagal";
		}
    }

	public function hapusdata(){
		$id=$this->uri->segment(4);
		$deletebyid=array('id_berita'=>$id);
		$this->suzuki_model->delete($deletebyid,'berita');
		header('location:'.base_url().'admin/berita');
	}
}
