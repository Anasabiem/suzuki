<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function index()
	{
		$data['navbar']='client/side/navbarHome';
		$data['footer']='client/side/footer';
		$data['contact']='client/contact_person';
		$data['product0']=$this->suzuki_model->clientProduct0();
		$data['product1']=$this->suzuki_model->clientProduct1();
		$data['product2']=$this->suzuki_model->clientProduct2();
		$data['product3']=$this->suzuki_model->clientProduct3();
		$data['product4']=$this->suzuki_model->clientProduct4();
		$data['video']=$this->suzuki_model->select('headline');
		$this->load->view('client/product', $data);
	}
	public function spesifikasi(){
		$data['navbar']='client/side/navbarHome';
		$data['footer']='client/side/footer';
		$data['contact']='client/contact_person';
		$id=$this->uri->segment(3);
		$data['mobil']=$this->suzuki_model->selectwhere('mobil',array('id_mobil'=>$id));
		$this->load->view('client/spesifikasi', $data);
	}
}
