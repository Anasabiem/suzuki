<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
function __construct()
    {
        parent::__construct();
        $this->load->model('suzuki_model');
  //       if($this->session->userdata('status') != "login"){
		// 	header('location:'.base_url().'');
		// }
    }
	public function index()
	{
		
		$data['navbar']='client/side/navbarHome';
		$data['footer']='client/side/footer';
		$data['layanan']='client/layanan';
		$data['berita']='client/berita';
		$data['location']='client/location';
		$data['galery']='client/galery';
		$data['contact']='client/contact_person';
    	$data['slider']=$this->suzuki_model->select('slider');
    	$data['berita']=$this->suzuki_model->selectlimit('berita');
    	$data['tampilgalery']=$this->suzuki_model->select('galery');
    	$data['product0']=$this->suzuki_model->clientProduct0();
		$data['product1']=$this->suzuki_model->clientProduct1();
		$data['product2']=$this->suzuki_model->clientProduct2();
		$data['product3']=$this->suzuki_model->clientProduct3();
		$data['product4']=$this->suzuki_model->clientProduct4();
		$this->load->view('client/home2', $data);
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => ($password)
			);
		$cek = $this->suzuki_model->cek_login("admin",$where)->num_rows();
		if($cek > 0){
			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 
			$this->session->set_userdata($data_session);
			redirect(base_url("admin/Beranda"));
 
		}else{
			echo "Username dan password salah !";
			redirect(base_url());
		}
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('Home'));
	}
	function detail_berita(){
		$id=$this->uri->segment(3);
		$data['berita']=$this->suzuki_model->selectlimit('berita');
		$data['dtberita']=$this->suzuki_model->selectwhere('berita',array('id_berita'=>$id));
		$this->load->view('client/d-berita', $data);
	}
	function berita(){
		$data['berita']=$this->suzuki_model->get_news('berita');
		$this->load->view('client/all-berita', $data);
	}
	public function spesifikasi(){
		$data['navbar']='client/side/navbarHome';
		$data['footer']='client/side/footer';
		$data['contact']='client/contact_person';
		$id=$this->uri->segment(3);
		$data['mobil']=$this->suzuki_model->selectwhere('mobil',array('id_mobil'=>$id));
		$this->load->view('client/spesifikasi', $data);
	}
}
