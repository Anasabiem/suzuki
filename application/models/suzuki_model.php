<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suzuki_model extends CI_Model {

    public function select($table){
    return $this->db->get($table);
  }
  public function selectlimit($table){
      $this->db->order_by('id_berita','DESC') ;
     $this->db->limit(6) ;
     return $this->db->get($table) ;
  }

  public function selectwhere($table,$data){
    return $this->db->get_where($table, $data);
  }

  function delete($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  }

  public function update($table,$data,$where){
    $this->db->update($table,$data,$where);
  }

  public function insert($table,$data){
    $this->db->insert($table,$data);
  }
  function cek_login($table,$where){
    return $this->db->get_where($table,$where);
  }

    function get_model(){
    $query = $this->db->query('SELECT * FROM automobil ORDER BY id_automobile ASC');
    return $query->result();
  }

  function get_automobile(){
  $query = $this->db->query('SELECT * FROM model ORDER BY id_model ASC');
  return $query->result();
}

  function get_mobil(){
    $query = $this->db->query('SELECT * FROM mobil,automobil WHERE automobil.id_automobile=mobil.id_automobile');
    return $query->result();
  }
  function get_news(){
    return $this -> db -> query('SELECT * FROM berita ORDER BY id_berita DESC');
    return $query->result ();
  }
  function get_galery(){
      return $this->db->query('SELECT * FROM galery ORDER BY id_galery DESC');
      return $query->result ();
  }

  //tampilan dashboard
        function dashboard_product(){
          // get total rows
          $this->db->from('mobil');
            return $this->db->count_all_results();
        }

        function dashboard_news(){
          // get total rows
          $this->db->from('berita');
            return $this->db->count_all_results();
        }

        function dashboard_slider(){
          // get total rows
          $this->db->from('galery');
            return $this->db->count_all_results();
        }

        function dashboard_video(){
          // get total rows
          $this->db->from('headline');
            return $this->db->count_all_results();
        }
  //end of tampilan dashboard

  //pricelist
        function pricelist(){
          $this->db->select('harga.*, model.*');
          $this->db->from('harga');
          $this->db->join('model', 'harga.id_model = model.id_model');
          // $this->db->where('harga.id_harga', '1');
          $data=$this->db->get();
          return $data;
        }

        function pricelistClient($id){
          $this->db->select('harga.*, model.*');
          $this->db->from('harga');
          $this->db->join('model', 'harga.id_model = model.id_model');
          $this->db->where('harga.id_model', $id);
          $data=$this->db->get();
          return $data;
        }

        function pricelistTambah(){
          return $this->db->get('model');
        }
  //end of pricelist

  //Product
        function editProduct($id){
          $this->db->select('mobil.*, automobil.*');
          $this->db->from('mobil');
          $this->db->join('automobil', 'mobil.id_automobile = automobil.id_automobile');
          $this->db->where('mobil.id_mobil', $id);
          $data=$this->db->get();
          return $data;
        }

        function clientProduct0(){
          $this->db->select('mobil.*, automobil.*');
          $this->db->from('mobil');
          $this->db->join('automobil', 'mobil.id_automobile = automobil.id_automobile');
          // $this->db->where('mobil.id_automobile', '1');
          $data=$this->db->get();
          return $data;
        }

        function clientProduct1(){
          $this->db->select('mobil.*, automobil.*');
          $this->db->from('mobil');
          $this->db->join('automobil', 'mobil.id_automobile = automobil.id_automobile');
          $this->db->where('mobil.id_automobile', '1');
          $data=$this->db->get();
          return $data;
        }

        function clientProduct2(){
          $this->db->select('mobil.*, automobil.*');
          $this->db->from('mobil');
          $this->db->join('automobil', 'mobil.id_automobile = automobil.id_automobile');
          $this->db->where('mobil.id_automobile', '2');
          $data=$this->db->get();
          return $data;
        }

        function clientProduct3(){
          $this->db->select('mobil.*, automobil.*');
          $this->db->from('mobil');
          $this->db->join('automobil', 'mobil.id_automobile = automobil.id_automobile');
          $this->db->where('mobil.id_automobile', '3');
          $data=$this->db->get();
          return $data;
        }

        function clientProduct4(){
          $this->db->select('mobil.*, automobil.*');
          $this->db->from('mobil');
          $this->db->join('automobil', 'mobil.id_automobile = automobil.id_automobile');
          $this->db->where('mobil.id_automobile', '4');
          $data=$this->db->get();
          return $data;
        }

        function get_mobilClient($id){
          $this->db->select('mobil.*');
          $this->db->where('id_mobil', $id);
          $data=$this->db->get();
          return $data;
        }
  //end of Product


}
