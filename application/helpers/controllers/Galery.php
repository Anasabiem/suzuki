<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galery extends CI_Controller {

	public function index()
	{
		$data['navbar']='client/side/navbarHome';
		$data['footer']='client/side/footer';
		$data['galery']=$this->suzuki_model->select('galery');
		$this->load->view('client/formgalery', $data);
	}
}
