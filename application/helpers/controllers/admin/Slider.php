<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
                // $this->load->helper(array('form', 'url'));
        }

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('Home');
		}else {
			$data['gambar'] = $this->suzuki_model->select('slider');
			$this->load->view('admin/slider', $data);
		}
	}

	public function editSlider(){
		$id=$this->uri->segment(4);
		$data['gambar'] = $this->suzuki_model->selectwhere('slider', array('id_slider' => $id));
		$data['error'] = ' ';
		$this->load->view('admin/editSlider', $data);
	}

	public function prosesEdit(){
				$config['upload_path']          = 'gallery/gambar_slider';
                $config['allowed_types']        = 'jpg|png|gif';
                $config['max_size']             = 0;
                $config['max_width']            = 0;
                $config['max_height']           = 0;
								// $date = date("His_dmY_");
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $this->load->view('admin/editSlider', $error);
                }
                else
                {
												$data = $this->upload->data();
											 	$name_file=$data['file_name'];
												$update_data['gambar_slider'] = $name_file;
												$id_slider=$this->input->post('id_slider');
												$update_data['judul_slider'] = $this->input->post('name');
												$update_data['isi_slider'] = $this->input->post('description');
												$this->suzuki_model->update('slider', $update_data, array('id_slider'=>$id_slider));
                        // $this->load->view('admin/Slider');
												header('location:'.base_url().'admin/Slider');
                }
	}
}
