<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
function __construct()
    {
        parent::__construct();
        $this->load->model('suzuki_model');
  //       if($this->session->userdata('status') != "login"){
		// 	redirect(base_url());
		// }
    }
	public function index()
	{
    if(!$this->session->userdata('status') == 'login'){
      redirect('Home');
    }else{
      $data['navbar']='admin/side/navbar';
  		$data['sidebar']='admin/side/sidebar';
  		$data['product'] = $this->suzuki_model->dashboard_product();
  		$data['news'] = $this->suzuki_model->dashboard_news();
  		$data['slider'] = $this->suzuki_model->dashboard_slider();
  		$data['video'] = $this->suzuki_model->dashboard_video();
  		$this->load->view('admin/home', $data);
    }
	}
}
