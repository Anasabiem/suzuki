<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galery extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('suzuki_model');
    }
	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
      redirect('Home');
    }else {
			$data['galery']=$this->suzuki_model->get_galery();
			$this->load->view('admin/galery',$data);
			// $this->load->view('admin/side/nav_berita');
    }
	}
	
	public function create(){
        if(isset($_POST['btnSimpan'])){
          $config = array('upload_path' => './gallery/gambar_galery/',
                  'allowed_types' => 'gif|jpg|png|jpeg'
                  );
          $this -> load -> library ('upload',$config);
          if ($this->upload->do_upload('gambar_galery'))
        {
            $upload_data = $this -> upload -> data ();
            $foto = "gallery/gambar_galery/".$upload_data['file_name'];
        $data = array(
        'nama_gambar' => $foto
        );
        $insert_data = $this->db->insert('galery',$data);
      }
      if ($insert_data) {
        redirect(base_url().'index.php/admin/Galery');
       } else{
        echo "string";
       }
    }else{
      echo "gagal";
    }
  }
  public function hapus($id){
    $where = array('id_galery'=>$id);
    $hapus = $this -> suzuki_model -> delete($where,'galery');
    if($hapus){
    header('location:'.base_url('index.php/admin/Galery')); ?>
    <div class="alert bg-red">
               Lorem ipsum dolor sit amet, id fugit tollit pro, illud nostrud aliquando ad est, quo e sse dolorum id
         </div>

            <?php
    }else{
      header('location:'.base_url('index.php/admin/Galery'));
      echo "gagal";
    }
  }
}
