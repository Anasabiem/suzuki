<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pricelist extends CI_Controller {

  public function index(){
    if(!$this->session->userdata('status') == 'login'){
      redirect('Home');
    }else {
      $data['pricelist']=$this->suzuki_model->pricelist();
      $this->load->view('admin/pricelist', $data);
    }
  }
  // asd

  public function tambahPricelist(){
    $data['pricelistTambah']=$this->suzuki_model->pricelistTambah();
    $this->load->view('admin/t-pricelist', $data);
  }

  public function insertPricelist(){
    // header('Content-type : aplication:json');
    $data['sales_model']=$this->input->post('sales_model');
    $data['harga']=$this->input->post('harga');
    $data['id_model']=$this->input->post('modelu');
    $this->suzuki_model->insert('harga', $data);
    header('location:'.base_url().'admin/Pricelist');
    // echo json_encode($data['id_model']->row());
  }

  public function editPricelist(){
		$id=$this->uri->segment(4);
		// $updatebyid=array('id_harga'=>$id);
    $data['sales_model'] = $this->input->post('sales_model');
    $data['harga'] = $this->input->post('harga');
    $data['id_model'] = $this->input->post('id_model');
		$this->suzuki_model->update('harga', $data, array('id_harga' => $id));
		header('location:'.base_url().'admin/Pricelist');
	}

  public function deletePricelist(){
		$id=$this->uri->segment(4);
		$deletebyid=array('id_harga'=>$id);
		$this->suzuki_model->delete($deletebyid, 'harga');
		header('location:'.base_url().'admin/Pricelist');
	}

}
