<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_person extends CI_Controller {

	public function index()
	{
		$data['navbar']='client/side/navbarHome';
		$data['footer']='client/side/footer';
		$this->load->view('client/contact_person', $data);
	}
}
