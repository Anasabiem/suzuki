<?php $this->load->view($navbar); ?>

<div id="heading-breadcrumbs">
  <div class="container-fluid">
    <div class="row d-flex align-items-center flex-wrap">
      <div class="col-md-7">
        <h1 class="h2">Detail Product</h1>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="jumbotron">
    <?php foreach ($mobil->result() as $key) { ?>
      <img class="col-sm-12" src="<?php echo base_url(); ?>gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Exterior">
    <?php } ?>
  </div>
</div>
<section class="bar" style="padding-top: 40px; padding-bottom: 20px;">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="panel panel-default sidebar-menu with-icons">
          <div class="panel-body">
            <ul class="nav nav-pills flex-column text-sm" role="tablist">
              <li><a data-toggle="pill" href="#1" class="nav-link active">Spesifikasi</a></li>
              <li><a data-toggle="pill" href="#2" class="nav-link">Fitur</a></li>
              <li><a data-toggle="pill" href="#3" class="nav-link">Video</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-9">
        <div  class="tab-content">
          <div id="1" class="tab-pane fade show active" role="tabpanel">
            <?php foreach ($mobil->result() as $key) { ?>
              <h1><?php echo $key->nama_mobil; ?></h1>
              <div class="row clearfix">
                <table class="table">
                  <tr>
                    <th scope="col">Panjang Keseluruhan</th>
                    <td><?php echo $key->spek_panjang; ?> mm</td>
                  </tr>
                  <tr>
                    <th scope="col">Lebar Keseluruhan</th>
                    <td><?php echo $key->spek_lebar; ?> mm</td>
                  </tr>
                  <tr>
                    <th scope="col">Tinggi Keseluruhan</th>
                    <td><?php echo $key->spek_tinggi; ?> mm</td>
                  </tr>
                  <tr>
                    <th scope="col">Tempat Duduk</th>
                    <td><?php echo $key->spek_tempat_duduk; ?> Orang</td>
                  </tr>
                  <tr>
                    <th scope="col">Tanki Bahan Bakar</th>
                    <td><?php echo $key->spek_tanki; ?> Liter</td>
                  </tr>
                </table>
              </div>
            <?php } ?>
          </div>
          <div id="2" class="tab-pane fade" role="tabpanel">
            <?php foreach ($mobil->result() as $key) { ?>
              <h2>Exterior</h2>
              <img class="col-sm-12" src="<?php echo base_url(); ?>gallery/gambar_product/<?php echo $key->gambar_exterior; ?>" alt="Exterior">
              <br><br>
              <h2>Interior</h2>
              <img class="col-sm-12" src="<?php echo base_url(); ?>gallery/gambar_product/<?php echo $key->gambar_interior; ?>" alt="Interior">
              <br><br>
              <h2>Performance</h2>
              <img class="col-sm-12" src="<?php echo base_url(); ?>gallery/gambar_product/<?php echo $key->gambar_performance; ?>" alt="Performance">
              <br><br>
              <h2>Safety</h2>
              <img class="col-sm-12" src="<?php echo base_url(); ?>gallery/gambar_product/<?php echo $key->gambar_safety; ?>" alt="Safety">
              <br><br>
            <?php } ?>
          </div>
          <div id="3" class="tab-pane fade" role="tabpanel">
            <?php foreach ($mobil->result() as $key) { ?>
              <h2>Video Product</h2>
              <iframe class="col-sm-12" height="480" src="<?php echo $key->embedcode_video_mobil; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div>
</section>
<?php $this->load->view($footer); ?>
<?php $this->load->view('client/side/js'); ?>
