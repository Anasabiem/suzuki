<hr>
<div style="margin-top: 10px; text-align: center;" >
  <h1 style="font-family: sans-serif;">LAYANAN</h1>
</div>
<hr>
<section class="bar background-white">
    <div class="container text-center">
        <div class="row">
          <div class="col-lg-4 col-md-6">
              <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-hand-o-up" style="margin-top: 30px;"></i></div>
                  <h3 class="h4">Antar Jemput Service</h3>
                  <p>Untuk anda yang tinggal di daerah Jember kota, kami menyediakan layanan antar jemput service saat anda sibuk dengan aktifitas sehari-hari.</p>
                  <li style="color: #7f8c8d"class="fa fa-pencil">
                    <i>Syarat & ketentuan berlaku</i>
                  </li>
              </div>
          </div>
          <div class="col-lg-4 col-md-6">
              <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-home" style="padding-top: 30px;" ></i></div>
                  <h3 class="h4">Home Service</h3>
                  <p>Anda sibuk dan tidak punya waktu untuk service kendaraan anda?? jangan khawatir, kami akan segera datang ke rumah anda, hubungi saya segera.</p>
                  <li style="color: #7f8c8d"class="fa fa-pencil">
                    <i>Syarat & ketentuan berlaku</i>
                  </li>
              </div>
          </div>
          <div class="col-lg-4 col-md-6">
              <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-user" style="padding-top: 30px;"></i></div>
                  <h3 class="h4">Layanan Darurat</h3>
                  <p>Mobil anda tiba-tiba macet di jalan? don't worry be happy, hubungi kami dan layanan darurat kami siap melayani anda setiap saat.</p>
                  <li style="color: #7f8c8d"class="fa fa-pencil">
                    <i>Syarat & ketentuan berlaku</i>
                  </li>
              </div>
          </div>
        </div>
    </div>
</section>
<section>
    <div style="margin-top: " class="container text-center">
        <div class="row">
            <div class="col-lg-4 col-md-6">
              <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-gear" style="padding-top: 30px;"></i>
                  </div>
                  <h3 class="h4">Garansi</h3>
                  <p>Kami memberikan garansi 3 tahun bagi anda pemilik mobil Suzuki baru, syarat dan ketentuan berlaku.</p>
                  <div style="margin-top: 29px">
                    <li style="color: #7f8c8d"class="fa fa-pencil">
                      <i>Syarat & ketentuan berlaku</i>
                    </li>
                  </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-heart-o" style="padding-top: 30px;"></i></div>
                  <h3 class="h4">Gratis Jasa Service</h3>
                  <p>Gratis jasa service sampai dengan 50.000km.</p>
                  <div style="margin-top: 50px">
                    <li style="color: #7f8c8d"class="fa fa-pencil">
                      <i>Syarat & ketentuan berlaku</i>
                    </li>
                  </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="box-simple">
                  <div class="icon-outlined"><i class=" fa fa-rocket" style="padding-top: 30px;"></i></div>
                  <h3 class="h4">Layanan Penjemputan</h3>
                  <p>Untuk anda yang tinggal di kota Jember, Gratis Jasa Penjemputan pada saat Delivery process di kantor Suzuki UMC Jember.</p>
                  <div style="margin-top: -12px">
                    <li style="color: #7f8c8d"class="fa fa-pencil">
                      <i style="margin-top: 10%">Syarat & ketentuan berlaku</i><br/>
                    </li>
                  </div>
                </div>
            </div>
       </div>
    </div>
</section>
