<?php $this->load->view($navbar); ?>

<div id="heading-breadcrumbs">
  <div class="container">
    <div class="row d-flex align-items-center flex-wrap">
      <div class="col-md-7">
        <h1 class="h2">Price list</h1>
      </div>
    </div>
  </div>
</div>
<section class="bar" style="padding-top: 40px; padding-bottom: 20px;" >
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="panel panel-default sidebar-menu with-icons">
          <div class="panel-body">
            <ul class="nav nav-pills flex-column text-sm" role="tablist">
              <li><a data-toggle="pill" href="#1" class="nav-link active">All New Ertiga</a></li>
              <li><a data-toggle="pill" href="#2" class="nav-link">Ignis</a></li>
              <li><a data-toggle="pill" href="#3" class="nav-link">Ignis Sport Edition</a></li>
              <li><a data-toggle="pill" href="#4" class="nav-link">Baleno</a></li>
              <li><a data-toggle="pill" href="#5" class="nav-link">New SX4 S-Cross</a></li>
              <li><a data-toggle="pill" href="#6" class="nav-link">Karimun Wagon R</a></li>
              <li><a data-toggle="pill" href="#7" class="nav-link">All New Grand Vitara 2.4</a></li>
              <li><a data-toggle="pill" href="#8" class="nav-link">APV</a></li>
              <li><a data-toggle="pill" href="#9" class="nav-link">Carry Pick Up</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-9">
        <div  class="tab-content">
          <div id="1" class="tab-pane fade show active" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarErtiga->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($newertiga->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
          <div id="2" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarIgnis->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
              <table class="table">
                <thead>
                  <tr>
                    <th>Group Model</th>
                    <th>Sales Model</th>
                    <th>Harga on the Road</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($ignis->result() as $key): ?>
                    <tr>
                      <td><?php echo $key->nama_model; ?></td>
                      <td><?php echo $key->sales_model; ?></td>
                      <td><?php echo $key->harga; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
          <div id="3" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarIgnisSport->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($ignisse->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
          <div id="4" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarBaleno->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($baleno->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
          <div id="5" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarSCross->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($newx->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
          <div id="6" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarKarimun->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($karimun->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
          <div id="7" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarGrandVit->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($vitara->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
          <div id="8" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarAPV->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($apv->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
          <div id="9" class="tab-pane fade" role="tabpanel">
            <div class="container">
              <?php foreach ($gambarCarry->result() as $key) { ?>
                <img src="<?php echo base_url() ?>gallery/gambar_product/<?php echo $key -> gambar_mobil ?>" width="100%">
              <?php } ?>
              <br>
              <br>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Group Model</th>
                      <th>Sales Model</th>
                      <th>Harga on the Road</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($carry->result() as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_model; ?></td>
                        <td><?php echo $key->sales_model; ?></td>
                        <td><?php echo $key->harga; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div> 

</section>

<?php $this->load->view($footer); ?>
<?php $this->load->view('client/side/js'); ?>
