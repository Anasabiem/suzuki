<?php $this->load->view($navbar); ?>
<section style="background: url('<?php echo base_url() ?>master/dist/img/photogrid.jpg') center center repeat; background-size: cover;" class="bar background-white relative-positioned">
  <div class="container">
    <!-- Carousel Start-->
    <div class="home-carousel">
      <div class="dark-mask mask-primary"></div>
      <div class="container">
        <div class="homepage owl-carousel">
          <?php foreach ($slider->result() as $s) {?>
          <div class="item">
            <div class="row">
              <div class="col-md-5 text-right">
                <p><img src="<?php echo base_url() ?>master/dist/img/logo.png" alt="" class="ml-auto"></p>
                <h1><?php echo $s->judul_slider; ?></h1>
                <p><?php echo $s->isi_slider; ?></p>
              </div>
              <div class="col-md-7"><img style="width: 600px;" src="<?php echo base_url() ?>gallery/gambar_slider/<?php echo $s->gambar_slider; ?>" alt="" class="img-fluid"></div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <!-- Carousel End-->
  </div>

  <!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div> 
</section>

<?php $this->load->view("client/layanan"); ?>
<?php $this->load->view($galery); ?>
          <!-- Javascript files-->
<?php $this->load->view($footer); ?>
<?php $this->load->view('client/side/js'); ?>
