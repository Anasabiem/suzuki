<?php $this->load->view("client/side/navbarHome"); ?>
<div id="heading-breadcrumbs" class="brder-top-0 border-bottom-0">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
              <h1 class="h2">Semua Berita</h1>
            </div>
            <div class="col-md-5">
              <ul style="font-weight: bold;" class="breadcrumb d-flex justify-content-end">
                <li><a href="<?php echo base_url() ?>">Home</a> > </li>
                <li> Berita</li>
                <!-- <li><a href="<?php echo base_url('Home/detail_berita') ?>"> Detail Berita </a></li> -->
              </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row" style="margin-top: 2%">
  <?php                                          
          foreach ($berita->result() as $i ) {
        ?>
  <div class="col-md-3">
    <div class="card profile-card-4">
        <div class="card-img-block">
            <img class="card-img-top" style="height: 290px;" src="<?php echo base_url().$i -> gambar_berita ?>" alt="Card image cap">
        </div>
        <div class="card-body pt-0">
          <br>
            <h5 class="card-title"><?php echo substr($i-> judul_berita,0,45);?>...</h5>
            <p class="card-text"><?php echo substr($i-> isi_berita,0,200); ?>...</p>
            <p class="card-text"><?php echo ($i-> sumber);?></p>
            <strong>
              <p style="text-align: right; font-style: italic; color: #7f8fa6; font-size: 13px"><?php echo date("d F Y", strtotime($i -> tgl_berita)) ?></p>
            </strong>
            <a href="<?php echo base_url()?>Home/detail_berita/<?php echo $i -> id_berita; ?>" class="btn btn-primary">Lanjut Baca</a>
        </div>
    </div>
  </div>
  <?php } ?>
  </div>
</div>

<!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div> 

<?php $this->load->view("client/side/footer"); ?>
<?php $this->load->view('client/side/js'); ?>