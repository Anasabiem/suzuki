<!-- FOOTER -->
      <footer class="main-footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-3">
              <h4 class="h6">Tentang Kami</h4>
              <p>Kami adalah salah satu perusahaan dealer Suzuki Jember, PT . UNITED MOTOR CENTER</p>
              <hr>
              <h4 class="h4">Way of life !!!</h4>
              <hr class="d-block d-lg-none">
            </div>
            <div class="col-lg-1">
              <hr class="d-block d-lg-none">
            </div>
            <div class="col-lg-3">
              <h4 class="h6">Hubungi Kami</h4>
              <p class="text-uppercase"><strong>Arif Witono</strong><br>Arif.umcjbr@gmail.com <br>Jl . Hayam Wuruk No. 2 Jember <br>PT . UNITED MOTOR CENTER <br><strong>Indonesia</strong></p><a href="<?php echo base_url("Contact_person")?>" class="btn btn-template-main"> Hubungi Kami</a>
              <hr class="d-block d-lg-none">
            </div>
            <div class="col-lg-5">
              <ul class="list-inline photo-stream">
                <div style="margin-right:30%">
                  <img width="120%" style="margin-left: 25%;margin-top: -15%" src="<?php echo base_url() ?>master/dist/img/suzuki1.png">
                  <br>
                </div>
              </ul>
            </div>
          </div>
        </div>
        <div class="copyrights" >
          <div class="container" >
            <div class="row" style="margin-bottom: -5%">
              <div class="col-lg-4 text-center-md">
                <p>&copy; 2018. Suzuki Jember </p>
              </div>
              <div class="col-lg-8 text-right text-center-md">
                <p> Developed by <a href="http://jemberkita.online" style="color: #4fbfa8 !important; font-weight: bold;"> Jemberkita </a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </div>
      </footer>