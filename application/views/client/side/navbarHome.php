<?php $this->load->view("client/side/head"); ?>
<div id="all">
  <!-- Top bar-->
      <div class="top-bar">
        <div class="container">
          <div class="row d-flex align-items-center">
            <div class="col-md-6 d-md-block d-none">
              <p style="font-size: 12px;">
                Hubungi Saya : <a href="https://api.whatsapp.com/send?phone=628123463120" style="color: #ffffff;font-weight: bold;"><i class="fa fa-whatsapp"></i>  +628123463120  </a>   atau   <a href="mailto:Arif.umcjbr@gmail.com" style="color: #ffffff; font-weight: bold;">  <i class="fa fa-envelope"></i> Arif Witono</a>
              </p>

            </div>
            <div class="col-md-6">
              <div class="d-flex justify-content-md-end justify-content-between">
                <ul class="list-inline contact-info d-block d-md-none">
                  <li class="list-inline-item"><a href="https://api.whatsapp.com/send?phone=628123463120"><i class="fa fa-phone"></i></a></li>
                  <li class="list-inline-item"><a href="mailto:Arif.umcjbr@gmail.com"><i class="fa fa-envelope"></i></a></li>
                </ul>

                <ul class="social-custom list-inline">
                  <li class="list-inline-item"><a href="#"><i style="margin-top: 9px" class="fa fa-facebook"></i></a></li>
                  <li class="list-inline-item"><a href="#"><i style="margin-top: 9px" class="fa fa-google-plus"></i></a></li>
                  <li class="list-inline-item"><a href="#"><i style="margin-top: 9px" class="fa fa-twitter"></i></a></li>
                  <li class="list-inline-item"><a href="#"><i style="margin-top: 9px" class="fa fa-envelope"></i></a></li>
                </ul>
                <div class="login" style="margin-left: 30px;">
                  <a href="" data-toggle="modal" data-target="#login-modal" class="login-btn">
                    <i style="margin-top: 9px" class="fa fa-sign-in"></i>
                    <span class="d-none d-md-inline-block">Sign In</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Top bar end-->
      <!-- Login Modal-->
      <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="modal fade">
        <div role="document" class="modal-dialog" style="background: #bdc3c7">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h4 id="login-modalLabel" class="modal-title">Admin Login</h4> -->
              <img style="margin-left: 10%" width="80%" src="<?php echo base_url() ?>master/dist/img/logo.png">
              <!-- <H2>LOGIN FORM</H2> -->
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url('Home/aksi_login'); ?>" method="post">
                <div class="form-group">
                  <input id="email_modal" type="text" placeholder="Username" class="form-control" name="username">
                </div>
                <div class="form-group">
                  <input id="password_modal" type="password" placeholder="password" class="form-control" name="password">
                </div>
                <p class="text-center">
                  <button class="btn btn-template-outlined" name="btnLogin"><i class="fa fa-sign-in"></i> Log in</button>
                </p>
              </form>
              <p class="text-center text-muted">SUZUKI JEMBER FORM LOGIN</p>
            </div>
          </div>
        </div>
      </div>
      <!-- Login modal end-->
      <!-- Navbar Start-->
      <header class="nav-holder make-sticky">
        <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
          <div class="container"><a href="Home" class="navbar-brand home"><img src="<?php echo base_url() ?>master/dist/img/download.jpg" alt="Universal logo" class="d-none d-md-inline-block"><img src="<?php echo base_url() ?>master/dist/img/download1.jpg" alt="Suzuki Jember" class="d-inline-block d-md-none"><span class="sr-only">Go to homepage</span></a>
            <button type="button" data-toggle="collapse" data-target="#navigation" class="navbar-toggler btn-template-outlined"><span class="sr-only">Toggle navigation</span><i class="fa fa-align-justify"></i></button>
            <div id="navigation" class="navbar-collapse collapse">
              <ul class="nav navbar-nav ml-auto">
               <li class="<?php if($this->uri->segment('1')=="" || $this->uri->segment('1')=="Home" ){ echo "active";}?>">
                  <a href="<?php echo base_url() ?>"> Home <b class="caret"></b></a>
                </li>
                <li class="<?php if($this->uri->segment('1')=="Product"){ echo "active";}?> nav-item dropdown menu-large"><a href="<?php echo base_url('Product')?>" >Product <b class="caret"></b></a>
                </li>
                <li class="<?php if($this->uri->segment('1')=="Price"){ echo "active";}?> nav-item dropdown menu-large">
                  <a href="<?php echo base_url('Price')?>" >Price List <b class="caret"></b></a>
                  <!-- <a href="#" data-toggle="dropdown" class="dropdown-toggle">Price List <b class="caret"></b></a>
                  <ul class="dropdown-menu megamenu">
                    <li>
                      <div class="row">
                        <div class="col-lg-6">
                          <img src="<?php echo base_url() ?>master/dist/img/template-homepage.png" alt="" class="img-fluid d-none d-lg-block">
                        </div>
                        <div class="col-lg-6 col-md-6">
                          <h5>Price List</h5>
                          <ul class="list-unstyled mb-3">
                            <li class="nav-item">
                              <a href="<?php echo base_url('Price')?>" class="nav-link">All Price List</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul> -->
                </li>
                <!-- ========== FULL WIDTH MEGAMENU ==================-->

                <li class="<?php if($this->uri->segment('1')=="Galery"){ echo "active";}?>">
                  <a href="<?php echo base_url('Galery')?>" >Gallery <b class="caret"></b></a>
                </li>

                <li class="<?php if($this->uri->segment('1')=="Contact_person"){ echo "active";}?>">
                  <a href="<?php echo base_url('Contact_person')?>" >Contact <b class="caret"></b></a>
                </li>
                <!-- ========== Contact dropdown end ==================-->
              </ul>
            </div>
            <div id="search" class="collapse clearfix">
              <form role="search" class="navbar-form">
                <div class="input-group">
                  <input type="text" placeholder="Search" class="form-control"><span class="input-group-btn">
                    <button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button></span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </header>
      <!-- Navbar End-->
  </div>
