<script src="<?php echo base_url() ?>master/dist/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>master/dist/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="<?php echo base_url() ?>master/dist/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>master/dist/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?php echo base_url() ?>master/dist/vendor/waypoints/lib/jquery.waypoints.min.js"> </script>
    <script src="<?php echo base_url() ?>master/dist/vendor/jquery.counterup/jquery.counterup.min.js"> </script>
    <script src="<?php echo base_url() ?>master/dist/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>master/dist/vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.min.js"></script>
    <script src="<?php echo base_url() ?>master/dist/js/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo base_url() ?>master/dist/vendor/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>master/dist/vendor/jquery.scrollto/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url() ?>master/dist/js/front.js"></script>

    <!-- floating  -->
    <script src="<?php echo base_url() ?>master/dist/js/index.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.5.0/velocity.min.js"></script> -->