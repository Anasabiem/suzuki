<?php $this->load->view($navbar); ?>
<div id="heading-breadcrumbs" class="brder-top-0 border-bottom-0">
        <div class="container">
          <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
              <h1 class="h2">Contact</h1>
            </div>
            <div class="col-md-5">
              <ul class="breadcrumb d-flex justify-content-end">
                
              </ul>
            </div>
          </div>
        </div>
      </div>
<div id="content">
        <div id="contact" class="container">
          <section class="bar">
            <div class="row">
              <div class="col-md-12">
                <div class="heading" style="margin-top: 30px;">
                  <h2>Kami disini untuk membantumu</h2>
                </div>
                <p class="lead">Ingin lebih tau lebih lanjut tentang suzuki ? anda mengalami masalah ketika memilih produk ? anda belum mengerti tentang produk kami ? jangan risau ! segera hubungi kami. kami akan membantu menjawab semua persoalan anda !</p>
                <p class="text-sm">layanan kami 24 jam untuk pelanggan..</p>
              </div>
            </div>
          </section>
          <section>
            <div class="row text-center">
              <div class="col-md-4">
                <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-map-marker" style="padding-top: 25px;"></i></div>
                  <h3 class="h4">Address</h3>
                  <p>Jalan Hayam Wuruk No.2
                    <br>Jember,<strong>Jawa Timur</strong></p>
                    <a style="font-weight: bold;" href="https://www.google.com/maps/place/PT.+UMC+Jember/@-8.1821901,113.6676422,208m/data=!3m1!1e3!4m5!3m4!1s0x0:0xfc38e661b2e04bd9!8m2!3d-8.182088!4d113.667212"><i class="fa fa-map-marker"></i> Telusuri lokasi</a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-phone" style="padding-top: 25px;"></i></div>
                  <h3 class="h4">Call center</h3>
                  <p>Nomor ini di gunakan untuk menanyakan proses pelayanan dan pemesanan mobil serta segala sesuatu yang berhubungan dengan suzuki di kabupaten Jember.</p>
                  <a href="https://api.whatsapp.com/send?phone=628123463120" style="font-weight: bold;"><i class="fa fa-whatsapp"></i>  +628123463120  </a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="box-simple">
                  <div class="icon-outlined"><i class="fa fa-envelope" style="padding-top: 25px;"></i></div>
                  <h3 class="h4">Electronic support</h3>
                  <p>Contact lain yang bisa di gunakan adalah dengan email.</p>
                  <ul class="list-unstyled text-sm">
                    <li><strong><a href="mailto:"><i class="fa fa-envelope"></i> Arif.umcjbr@gmail.com</a></strong></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>

<!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div> 

<?php $this->load->view($footer); ?>
<?php $this->load->view('client/side/js'); ?>