<?php $this->load->view("client/side/navbarHome"); ?>
<div id="heading-breadcrumbs" class="brder-top-0 border-bottom-0">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
              <h1 class="h2">Detail Berita</h1>
            </div>
            <div class="col-md-5">
              <ul style="font-weight: bold;" class="breadcrumb d-flex justify-content-end">
                <li><a href="<?php echo base_url() ?>"> Home </a> > </li>
                <li><a href="<?php echo base_url('Home/berita') ?>"> Berita </a></li>
              </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
  <?php
          foreach ($dtberita->result() as $i ) {
      ?>
<div class="card">
  <div class="container">
    <div style="margin-top: 5%;" class="col-md-12">
        <h2 style="text-align: center; color: #192a56"><?php echo ($i-> judul_berita) ?></h2>
        <p style="text-align: center; color: #718093"><?php echo date("d F Y", strtotime($i -> tgl_berita)) ?>
        </p>
        <center>
            <img style="width: 50%;" src="<?php echo base_url().$i -> gambar_berita ?>">
        </center>
    </div>
  </div>

    <div class="container">
    <p><?php echo ($i-> isi_berita) ?></p>
    </div>
</div>
  <?php } ?>
</div>
<hr>
<h1 style="text-align: center;">BERITA LAINYA</h1>
<hr>
<div class="container-fluid">
<!-- Carousel Start-->
    <ul class="owl-carousel testimonials list-unstyled equal-height">
      <?php
          foreach ($berita->result() as $i ) {
      ?>
      <li class="item card" style="height: 68%">
        <div class="testimonial d-flex flex-wrap">
          <div class="text">
            <img class="card-img-top" style="height: 290px; border-radius: 5px; min-height: 190px;" src="<?php echo base_url().$i -> gambar_berita ?>" alt="Card image cap">
            <br>
            <a href="<?php echo base_url()?>Home/detail_berita/<?php echo $i -> id_berita; ?>">
              <h4><?php echo substr($i-> judul_berita,0,45); ?>...</h4>
            </a>
          </div>
          <div class="bottom d-flex align-items-center justify-content-between align-self-end">
            <div class="icon"><i class="fa fa-quote-left"></i></div>
            <div class="testimonial-info d-flex">
              <div class="title">
                <p><?php echo date("d F Y", strtotime($i -> tgl_berita)) ?></p>
              </div>
              <div class="avatar" style="margin-top: 10%">
                  <img alt="" src="<?php echo base_url()?>master/dist/img/calendar.png" class="img-fluid">
              </div>
            </div>
          </div>
        </div>
      </li>
      <?php } ?>
    </ul>
    <!-- Carousel End-->
  </div>
  
  <!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div> 
<?php $this->load->view("client/side/footer"); ?>
<?php $this->load->view('client/side/js'); ?>
