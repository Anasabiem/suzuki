<div id="content">
        <div class="container">
          <div class="row bar">
            <div class="col-md-12">
              <div class="products-big">
                <div class="row products">
                  <div class="col-lg-3 col-md-4">
                    <div class="product">
                      <div class="image"><a href="shop-detail.html"><img src="<?php echo base_url() ?>master/dist/img/template-mac.png" alt="" class="img-fluid image1"></a></div>
                      <div class="text">
                        <h3 class="h5"><a href="shop-detail.html">Fur coat with very but very very long name</a></h3>
                        <p class="price">$143.00</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-4">
                    <div class="product">
                      <div class="image"><a href="shop-detail.html"><img src="<?php echo base_url() ?>master/dist/img/template-mac.png" alt="" class="img-fluid image1"></a></div>
                      <div class="text">
                        <h3 class="h5"><a href="shop-detail.html">White Blouse Armani</a></h3>
                        <p class="price">
                          <del>$280</del> $143.00
                        </p>
                      </div>
                      <div class="ribbon-holder">
                        <div class="ribbon sale">SALE</div>
                        <div class="ribbon new">NEW</div>
                      </div>
                    </div>
                  </div>
                  
              </div>
              <div class="row">
                <div class="col-md-12 banner mb-small text-center"><a href="#"><img src="<?php echo base_url() ?>master/dist/img/boxed-pattern" alt="" class="img-fluid"></a></div>
              </div>
              <div class="pages">
                <p class="loadMore text-center"><a href="#" class="btn btn-template-outlined"><i class="fa fa-chevron-down"></i> Load more</a></p>
                <nav aria-label="Page navigation example" class="d-flex justify-content-center">
                  <ul class="pagination">
                    <li class="page-item"><a href="#" class="page-link"> <i class="fa fa-angle-double-left"></i></a></li>
                    <li class="page-item active"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">4</a></li>
                    <li class="page-item"><a href="#" class="page-link">5</a></li>
                    <li class="page-item"><a href="#" class="page-link"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>