<?php $this->load->view($navbar); ?>

<div id="heading-breadcrumbs" class="brder-top-0 border-bottom-0">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
              <h1 class="h2">Gallery</h1>
            </div>
            <div class="col-md-5">
              <ul class="breadcrumb d-flex justify-content-end"></ul>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="container gallery-container" style="margin-top: 30px; margin-bottom: 30px;">
        <div class="tz-gallery">
            <div class="row mb-3">
                <?php foreach ($galery->result() as $s) {?>
                    <div class="col-md-4">
                        <div class="card">
                            <a class="lightbox" href="<?php echo $s->nama_gambar; ?>" >
                            <img style="height: 200px;" src="<?php echo $s->nama_gambar; ?>" alt="Park" class="card-img-top" data-sub-html="Click untuk Menghapus">
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div> 

<?php $this->load->view($footer); ?>
<?php $this->load->view('client/side/js'); ?>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
<script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>