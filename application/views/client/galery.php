<hr>
<div style="margin-top: 10px; text-align: center;" >
  <h1 style="font-family: sans-serif;">PRODUCT</h1>
</div>
<hr>
<div class="container-fluid">
<div class="container">
    <ul class="nav nav-tabs nav-pills nav-justified justify-content-center" role="tablist">
      <li style="padding:1%;"><a href="#ALL" data-toggle="pill" class="nav-link">SEMUA PRODUCT</a></li><br><br><br>
    </ul>
  </div>
      <div class="row d-flex align-items-stretch same-height">
        <?php foreach ($product0->result() as $key) { ?>
        <div class="col-md-4">
          <div class="card text-center" style="width: 18rem;">
              <img style="height:150px;width:100%;" class="card-img-top" src="gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Gambar Mobil">
            <div class="card-body">
              <h5 class="card-title"><?php echo $key->nama_mobil; ?></h5>
              <a href="<?php echo base_url('Product/spesifikasi/'.$key->id_mobil); ?>" class="btn btn-primary">Lihat</a>
            </div>
          </div>
        </div>
        <?php } ?>
      
</div>
</div>

<hr>
<div style="margin-top: 10px; text-align: center;" >
  <h1 style="font-family: sans-serif;">BERITA</h1>
</div>
<hr>
<div class="container-fluid">
<!-- Carousel Start-->
    <ul class="owl-carousel testimonials list-unstyled equal-height">
      <?php                                          
          foreach ($berita->result() as $i ) {
          ?>
      <li class="item card" style="height: 68%">
        <div class="testimonial d-flex flex-wrap">
          <div class="text">
            <img class="card-img-top" style="height: 290px; border-radius: 5px; min-height: 190px;" src="<?php echo base_url().$i -> gambar_berita ?>" alt="Card image cap">
            <br>
            <a href="<?php echo base_url()?>Home/detail_berita/<?php echo $i -> id_berita; ?>">
              <h4><?php echo substr($i-> judul_berita,0,45); ?>...</h4>
            </a>
          </div>
          <div class="bottom d-flex align-items-center justify-content-between align-self-end">
            <div class="icon"><i class="fa fa-quote-left"></i></div>
            <div class="testimonial-info d-flex">
              <div class="title">
                <p><?php echo date("d F Y", strtotime($i -> tgl_berita)) ?></p>
              </div>
              <div class="avatar" style="margin-top: 10%">
                  <img alt="" src="<?php echo base_url()?>master/dist/img/calendar.png" class="img-fluid">
              </div>
            </div>
          </div>
        </div>
      </li>
      <?php } ?>
    </ul>
    <!-- Carousel End-->           
</div>
<div class="pages">
  <p class="loadMore text-center"><a href="<?php echo base_url('Home/berita/') ?>" class="btn btn-template-outlined"><i class="fa fa-chevron-down"></i> Lihat Semua Berita</a></p>
</div>
<?php $this->load->view($location); ?>
