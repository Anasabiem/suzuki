<?php $this->load->view($navbar); ?>

  <div class="container-fluid" >
    <?php foreach ($video->result() as $key){ ?>
      <iframe class="col-sm-12" height="480" src="<?php echo $key->embedcode; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    <?php } ?>
  </div>
  <div style="text-align: center; margin-top: 10px; color: #173e81">
    <h2>Automobile Product</h2>
    <h4>Click for show product...</h4>
  </div>
  <div class="container">
    <ul class="nav nav-tabs nav-pills nav-justified justify-content-center" role="tablist">
      <li style="padding:1%;"><a href="#URBAN" data-toggle="pill" class="nav-link">URBAN SUV</a></li>
      <li style="padding:1%;"><a href="#PASSENGER" data-toggle="pill" class="nav-link">PASSENGER CAR</a></li>
      <li style="padding:1%;"><a href="#COMMERCIALS" data-toggle="pill" class="nav-link">COMMERCIALS CAR</a></li>
      <li style="padding:1%;"><a href="#SUV" data-toggle="pill" class="nav-link">SUV</a></li>
      <li style="padding:1%;"><a href="#ALL" data-toggle="pill" class="nav-link">ALL</a></li>
    </ul>
  </div>

  <div id="pills-tabContent" class="tab-content">
    <div id="URBAN" role="tabpanel" aria-labelledby="pills-home-tab" class="tab-pane fade">
        <div class="row d-flex align-items-stretch same-height">
          <?php foreach ($product1->result() as $key) { ?>
          <div class="col-md-4">
            <div class="card text-center" style="width: 18rem;">
                <img style="height:150px;width:100%;" class="card-img-top" src="gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Gambar Mobil">
              <div class="card-body">
                <h5 class="card-title"><?php echo $key->nama_mobil; ?></h5>
                <a href="<?php echo base_url('Product/spesifikasi/'.$key->id_mobil); ?>" class="btn btn-primary">Lihat</a>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
    </div>


    <div id="PASSENGER" role="tabpanel" aria-labelledby="pills-profile-tab" class="tab-pane fade">
      <div class="row d-flex align-items-stretch same-height">
        <?php foreach ($product2->result() as $key) { ?>
        <div class="col-md-4">
          <div class="card text-center" style="width: 18rem;">
              <img style="height:150px;width:100%;" class="card-img-top" src="gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Gambar Mobil">
            <div class="card-body">
              <h5 class="card-title"><?php echo $key->nama_mobil; ?></h5>
              <a href="<?php echo base_url('Product/spesifikasi/'.$key->id_mobil); ?>" class="btn btn-primary">Lihat</a>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>


    <div id="COMMERCIALS" role="tabpanel" aria-labelledby="pills-contact-tab" class="tab-pane fade">
      <div class="row d-flex align-items-stretch same-height">
        <?php foreach ($product3->result() as $key) { ?>
        <div class="col-md-4">
          <div class="card text-center" style="width: 18rem;">
              <img style="height:150px;width:100%;" class="card-img-top" src="gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Gambar Mobil">
            <div class="card-body">
              <h5 class="card-title"><?php echo $key->nama_mobil; ?></h5>
              <a href="<?php echo base_url('Product/spesifikasi/'.$key->id_mobil); ?>" class="btn btn-primary">Lihat</a>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>

    <div id="SUV" role="tabpanel" aria-labelledby="pills-marketing-tab" class="tab-pane fade">
      <div class="row d-flex align-items-stretch same-height">
        <?php foreach ($product4->result() as $key) { ?>
        <div class="col-md-4">
          <div class="card text-center" style="width: 18rem;">
              <img style="height:150px;width:100%;" class="card-img-top" src="gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Gambar Mobil">
            <div class="card-body">
              <h5 class="card-title"><?php echo $key->nama_mobil; ?></h5>
              <a href="<?php echo base_url('Product/spesifikasi/'.$key->id_mobil); ?>" class="btn btn-primary">Lihat</a>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>

    <div id="ALL" role="tabpanel" aria-labelledby="pills-marketing-tab" class="tab-pane fade">
      <div class="row d-flex align-items-stretch same-height">
        <?php foreach ($product0->result() as $key) { ?>
        <div class="col-md-4">
          <div class="card text-center" style="width: 18rem;">
              <img style="height:150px;width:100%;" class="card-img-top" src="gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Gambar Mobil">
            <div class="card-body">
              <h5 class="card-title"><?php echo $key->nama_mobil; ?></h5>
              <a href="<?php echo base_url('Product/spesifikasi/'.$key->id_mobil); ?>" class="btn btn-primary">Lihat</a>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
  </div>
</div>

<!-- floating button -->
  <div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn" style="background-color:#1e3799;">
      <i class="">
        <img style="width: 30px; color:#ffffff; margin-top: 25%" src="<?php echo base_url() ?>master/dist/img/send2.png">
      </i>
    </a>
    <ul class="zoom-menu">
      <li>
        <a href="tel:+628123463120" class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-phone" style="margin-top: 10px"></i>
        </a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
    </ul>
  </div>

<!-- <?php $this->load->view('client/variant'); ?> -->
<?php $this->load->view($footer); ?>
<?php $this->load->view('client/side/js'); ?>
