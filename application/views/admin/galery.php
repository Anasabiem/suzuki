<?php $this->load->view('admin/side/head'); ?>

<section class="content">
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <button type="button" style="width: 30%;" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#defaultModal">
                                  <i class="material-icons">add_to_queue</i>
                                  <span>TAMBAH FOTO</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row clearfix">
        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                GALLERY
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Gambar Galery</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1; 
                                        foreach ($galery->result() as $s) {?>
                                        <tr>
                                            <td><?php echo $no++?></td>
                                            <td><img src="<?php echo base_url().$s -> nama_gambar ?>" width="100"></td>
                                            <td>
                                                <a href="<?php echo base_url('index.php/admin/Galery/hapus/'.$s-> id_galery) ?>" class="btn btn-danger waves-effect " title="Hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')">
                                                    <i class="material-icons">delete_sweep</i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="row clearfix">
        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                     <div class="header">
                            <h2>
                                LIBRARY
                            </h2>
                        </div>    
                        <div class="body">
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <?php foreach ($galery->result() as $s) {?>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="<?php echo base_url().$s -> nama_gambar ?>" data-sub-html="Galery Suzuki Jember">
                                        <img style="height: 200px;" class="img-responsive thumbnail" 
                                        src="<?php echo base_url().$s -> nama_gambar ?>">
                                    </a>
                                </div>
                         <?php } ?>
                            </div>     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal Default -->
    <form autocomplete="off" method="post" action="<?php echo base_url(). 'index.php/admin/Galery/create'; ?>" enctype="multipart/form-data">
    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <b >Gambar Galery </b>
                        <div class="form" style="margin-top: 10px;">
                            <input class="btn btn-primary waves-effect" type="file" name="gambar_galery" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
            <button class="btn btn-primary waves-effect" type="submit" name="btnSimpan" value="upload" style="float: right; "><span>Simpan</span></button>
                </div>
            </div>
        </div>
    </div>
</form>
    <!-- end Modal Default -->

<?php $this->load->view('admin/side/navbar'); ?>
  <?php $this->load->view('admin/side/js'); ?>
