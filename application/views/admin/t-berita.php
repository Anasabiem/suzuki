<?php $this->load->view('admin/side/head'); ?>
<script src="<?php echo base_url() ?>master/admin/plugins/tinymce/tinymce.js"></script>
<!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script> -->
  <script>tinymce.init({ selector:'textarea' });</script>

<section class="content">
<div class="container-fluid">
    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TAMBAH BERITA</h2>
                            <ul class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url("admin/berita") ?>">Berita</a></li>
                              <li class="breadcrumb-item active">Tambah Berita</li>
                            </ul>
                        </div>
                        <div class="body">
                            <form autocomplete="off" method="post" action="<?php echo base_url(). 'admin/Berita/create'; ?>" enctype="multipart/form-data">
                                <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="judul" required>
                                            <label class="form-label">Judul Berita *</label>
                                        </div>
                                </div>
                                <div class="form-group form-float">
                                        <b>Tanggal Realise *</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="date" class="form-control date" placeholder="Ex: 30/07/2016" name="tanggal" required="">
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="sumber" required>
                                            <label class="form-label">Sumber</label>
                                        </div>
                                </div>
                                <div class="form-group form-float" >
                                    <b >Gambar Berita *</b>
                                    <div class="form" style="margin-top: 10px;">
                                        <input class="btn btn-primary waves-effect" type="file" name="berita" required>
                                    </div>
                                </div>
                                                    <!-- TinyMCE -->
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h5>ISI BERITA *</h5>
                                            <div >
                                                <textarea id="tinymce" name="isi" ></textarea>
                                            </div>

                                    </div>
                                </div>

                                <div style="margin-bottom: 20px;">
                                    <button class="btn btn-primary waves-effect" type="submit" name="btnSimpan" value="upload" style="float: right; width: 100px;"><span>Simpan</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<script type="text/javascript">
    function data(){
        swal("Data Tersimpan");
    }
</script>
  <?php $this->load->view('admin/side/navbar'); ?>
  <?php $this->load->view('admin/side/js'); ?>
