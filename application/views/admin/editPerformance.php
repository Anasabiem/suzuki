<?php $this->load->view('admin/side/head'); ?>
<?php $this->load->view('admin/side/navbar'); ?>
<?php $this->load->view('admin/side/js'); ?>
<section class="content">

  <!-- Basic Validation -->
  <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card">
              <div class="header">
                  <h2>Edit Gambar</h2>
              </div>
              <div class="body">
                  <?php echo form_open_multipart(base_url('admin/Product/prosesEditGambarPerformance/'));?>
                      <div class="form-group form-float">
                          <div class="form-line">
                            <?php foreach ($gambar_performance->result() as $judul) {?>
                              <input type="text" class="form-control" name="name" value="<?php echo $judul->nama_mobil; ?>" required disabled>
                              <input name="id_mobil" value="<?php echo $judul->id_mobil; ?>" hidden>
                            <?php } ?>
                              <label class="form-label">Judul</label>
                          </div>
                      </div>
                      <div class="form-group form-float">
                          <div class="form-line">
                              <input class="btn btn-primary waves-effect" type="file" name="userfile" required>
                          </div>
                      </div>
                      <button class="btn btn-primary waves-effect" type="submit" value="upload">Simpan</button>
                  </form>
                </div>
              </div>
          </div>
      </div>
  </div>
  <!-- #END# Basic Validation -->
</section>
