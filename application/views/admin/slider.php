<?php $this->load->view('admin/side/head'); ?>
 <section class="content">
<div class="container-fluid">
    <div class="row clearfix">
      <!-- Custom Content -->
      <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                  <div class="header">
                      <h2>
                          Edit Slider
                          <small>Untuk menampilkan gambar highlight di halaman Home.</small>
                      </h2>
                  </div>
                  <div class="body"  >
                      <div class="row">
                        <?php foreach ($gambar->result() as $key) { ?>
                          <div class="col-sm-6 col-md-3">
                              <div class="thumbnail" style="height: 350px;">
                                  <img style="height: 150px;" src="<?php echo  base_url()?>gallery/gambar_slider/<?php echo $key->gambar_slider; ?>">
                                  <div class="caption">
                                      <h3>Slider <?php echo $key->id_slider; ?></h3>
                                      <h6><?php echo $key->judul_slider; ?></h6>
                                      <p class="m-t-15"><?php echo $key->isi_slider; ?></p>
                                      
                                  </div>
                                  <div style="bottom: 0px;">
                                    <a href="<?php echo base_url(); ?>admin/Slider/editSlider/<?php echo $key->id_slider; ?>" class="btn btn-primary waves-effect" role="Edit"><i class="material-icons">create</i></a>
                                  </div>
                              </div>
                          </div>
                          <?php } ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- #END# Custom Content -->
    </div>
    </div>
</section>
<?php $this->load->view('admin/side/navbar'); ?>
<?php $this->load->view('admin/side/js'); ?>
