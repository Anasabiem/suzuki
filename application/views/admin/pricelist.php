<?php $this->load->view("admin/side/head"); ?>

<!-- Content -->
<!-- Exportable Table -->
<section class="content">
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        PRICE LIST EXPORTABLE
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                      <div style="margin-bottom: 25px;">
                        <a  href="<?php echo base_url("admin/Pricelist/tambahPricelist") ?>">
                        <button type="button" style="width: 30%;" class="btn btn-primary waves-effect">
                            <i class="material-icons">add_to_queue</i>
                            <span>TAMBAH PRODUCT</span>
                        </button>
                        </a>
                      </div>
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Group Model</th>
                                    <th>Sales Model</th>
                                    <th>Harga on the Road</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Group Model</th>
                                  <th>Sales Model</th>
                                  <th>Harga on the Road</th>
                                  <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php foreach ($pricelist->result() as $key) { ?>
                                <tr>
                                    <td><?php echo $key->nama_model; ?></td>
                                    <td><?php echo $key->sales_model; ?></td>
                                    <td><?php echo $key->harga; ?></td>
                                    <td style="text-align: center;">
                                        <a type="button" data-toggle="modal" data-target="#smallModal<?php echo $key->id_harga; ?>" class="btn btn-info btn-xs" >Update</a>
                                        <a class="btn btn-danger btn-xs" onclick="deleted('<?php echo $key->id_harga; ?>')">Delete</a></td>
                                </tr>
                                <!-- Small Size -->
                                <div class="modal fade" id="smallModal<?php echo $key->id_harga; ?>" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                          <?php echo form_open_multipart(base_url('admin/Pricelist/editPricelist/').$key->id_harga);?>
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="smallModalLabel">Edit Harga <?php echo $key->nama_model; ?></h4>
                                            </div>
                                            <div class="modal-body">
                                              <div class="form-group">
                                                  <div class="form-line">
                                                      <input name="sales_model" type="text" class="form-control" placeholder="Sales Model" value="<?php echo $key->sales_model; ?>" />
                                                      <input name="harga" type="text" class="form-control" placeholder="Harga" value="<?php echo $key->harga; ?>" />
                                                      <input name="id_model" value="<?php echo $key->id_model; ?>" hidden />
                                                  </div>
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
<!-- #END# Exportable Table -->



<!-- End of Content -->

<?php $this->load->view('admin/side/navbar'); ?>
<?php $this->load->view('admin/side/js'); ?>

<!-- JAVA SCRIPT KONFIRMASI HAPUS -->
<script type="text/javascript">
  function deleted(param){
    var proc = window.confirm('Apakah Anda yakin akan menghapus data?');
    if(proc){
      document.location='<?php echo base_url(); ?>admin/Pricelist/deletePricelist/'+param;
    }
  }
</script>
