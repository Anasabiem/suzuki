
<?php $this->load->view('admin/side/head'); ?>
<link href="<?php echo  base_url()?>master/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
              <div class="block-header">
                  <!-- <h2>
                      JQUERY DATATABLES
                  </h2> -->
              </div>
              <!-- Exportable Table -->
              <div class="row clearfix">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  SUZUKI PRODUCT
                              </h2>

                          </div>
                          <div class="body">
                              <div class="table-responsive">
                                <div style="margin-bottom: 25px;">
                                  <a href="<?php echo base_url('admin/Product/tambahData')?>">
                                  <button type="button" style="width: 30%;" class="btn btn-primary waves-effect">
                                      <i class="material-icons">add_to_queue</i>
                                      <span>TAMBAH PRODUCT</span>
                                  </button>
                                  </a>
                                </div>
                                  <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                      <thead>
                                          <tr>
                                              <th>No</th>
                                              <th>Nama Mobil</th>
                                              <th>Type/Model</th>
                                              <th>Gambar Mobil</th>
                                              <th>Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($mobil as $s) { ?>
                                          <tr>
                                              <td><?php echo $no++?></td>
                                              <td><?php echo $s-> nama_mobil?></td>
                                              <td><?php echo $s-> nama_automobile?></td>
                                              <td class="align-center"><img src="<?php echo base_url()?>gallery/gambar_product/<?php echo $s->gambar_mobil; ?>" width="100" height="100"></td>
                                              <td style="text-align: center;">
                                                  <a href="<?php echo base_url('admin/Product/editData/'.$s->id_mobil) ?>" class="btn btn-warning waves-effect " title="Detail" ><i class="material-icons">visibility</i>  Detail dan Edit</a>
                                                  <a href="<?php echo base_url('index.php/admin/Product/hapus/'.$s-> id_mobil) ?>" class="btn btn-danger waves-effect " title="Hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')"><i class="material-icons">delete_sweep</i>  Hapus</a>
                                                </td>
                                          </tr>
                                          <?php  } ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- #END# Exportable Table -->
    </div>
  </div>
</section>
  <?php $this->load->view('admin/side/navbar'); ?>
  <?php $this->load->view('admin/side/js'); ?>
