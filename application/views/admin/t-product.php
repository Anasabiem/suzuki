
<?php $this->load->view('admin/side/head'); ?>
<section class="content">
<!-- Input -->
    <div class="row clearfix">
        <form autocomplete="off" method="post" action="<?php echo base_url(). 'index.php/admin/Product/create'; ?>" enctype="multipart/form-data">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>TAMBAH PRODUCT</h2>
                    <ul class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="<?php echo base_url("admin/product") ?>">Product</a>
                      </li>
                      <li class="breadcrumb-item active">Tambah product</li>
                    </ul>
                </div>
                <div class="body">
                    <h2 class="card-inside-title">SPESIFIKASI MOBIL</h2>
                    <div class="row clearfix">
                        <div class="body">
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="nama_mobil" class="form-control" required />
                                        <label class="form-label">Nama Product Mobil</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <select class="form-control show-tick" name="id_automobile" required>
                                    <option value="">---- Pilih Automobile ----</option>
                                    <?php
                                      foreach ($model as $s) {
                                    ?>
                                    <option value="<?php echo $s->id_automobile ?>"><?php echo $s->nama_automobile; ?></option>
                                    <?php
                                      }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <select class="form-control show-tick" name="id_model" required>
                                    <option value="">---- Pilih Type/Model ----</option>
                                    <?php
                                      foreach ($automobile as $a) {
                                    ?>
                                    <option value="<?php echo $a->id_model; ?>"><?php echo $a->nama_model; ?></option>
                                    <?php
                                      }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="spesifikasi_panjang" required/>
                                        <label class="form-label">Spesifikasi Panjang</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="spesifikasi_lebar" required/>
                                        <label class="form-label">Spesifikasi Lebar</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="spesifikasi_tinggi" required/>
                                        <label class="form-label">Spesifikasi Tinggi</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="spesifikasi_tempat_duduk" required/>
                                        <label class="form-label">Spesifikasi Tempat Duduk</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="spesifikasi_tanki" required/>
                                        <label class="form-label">Spesifikasi Tanki</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea rows="5" type="text" class="form-control" name="url_video" required></textarea>
                                        <input name="gambar_mobil" value="default.jpg" hidden />
                                        <input name="gambar_exterior" value="default.jpg" hidden />
                                        <input name="gambar_interior" value="default.jpg" hidden />
                                        <input name="gambar_performance" value="default.jpg" hidden />
                                        <input name="gambar_safety" value="default.jpg" hidden />
                                        <label class="form-label">Embed Code Video</label>
                                    </div>

                                </div>
                                <div class="header bg-green">
                                    <h2>
                                        Catatan <small>untuk menuliskan embed</small>
                                    </h2>
                                    <br>
                                    <small>Contoh embed code:</small>
                                    <p>https://www.youtube.com/embed/44syBMDS2JA</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <button class="btn btn-primary" type="submit" title="Simpan" name="btnSimpan">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
            <!-- #END# Input -->
            <!-- Select -->
        </section>
         <?php $this->load->view('admin/side/navbar'); ?>
  <?php $this->load->view('admin/side/js'); ?>
