<?php $this->load->view('admin/side/head'); ?>
<section class="content">
    <!-- Textarea -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>EMBED YOUTUBE</h2>
                </div>
                <!-- Headline block -->
                <div class="body">
                  <?php echo form_open_multipart(base_url('admin/Video/prosesVideo/'));?>
                    <h2 class="card-inside-title">Code Embed SEBELUMNYA</h2>
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-line">
                                  <?php foreach ($embed->result() as $key) { ?>
                                    <iframe class="col-sm-12" height="480" src="<?php echo $key->embedcode; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                  <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                  </form>
                </div>
                <!-- Headline block -->
                <div class="body">
                  <?php echo form_open_multipart(base_url('admin/Video/prosesVideo/'));?>
                    <h2 class="card-inside-title">Code Embed</h2>
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="embed" rows="4" class="form-control no-resize" placeholder="Contoh https://www.youtube.com/embed/44syBMDS2JA"></textarea>
                                </div>
                                <br>
                                <p>Contoh embed code:</p>
                                <textarea rows="4" class="form-control no-resize"><iframe width="853" height="480" src="https://www.youtube.com/embed/DZYKscqmc1Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>hanya copy urlnya https://www.youtube.com/embed/DZYKscqmc1Y</textarea>
                                <p>hanya copy urlnya <b>https://www.youtube.com/embed/DZYKscqmc1Y</b></p>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect">Simpan</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Textarea -->
</section>
  <?php $this->load->view('admin/side/navbar'); ?>
  <?php $this->load->view('admin/side/js'); ?>
