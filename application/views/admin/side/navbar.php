<!DOCTYPE html>
<html>
<body class="theme-indigo">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url('admin/Beranda')  ?>">SUZUKI JEMBER</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="pull-right"><a href="<?php echo base_url('Home/logout'); ?>" class="js-right-sidebar" onclick="" data-close="true"><i class="material-icons">power_settings_new</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
                    <!-- User Info -->
                    <div class="user-info">
                        <div class="image">
                            <img src="<?php echo  base_url()?>master/admin/images/user.png" width="48" height="48" alt="User" />
                        </div>
                        <div class="info-container">
                            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</div>
                            <div class="email">Arif.umcjbr@gmail.com</div>
                            <div class="btn-group user-helper-dropdown">
                                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                                    <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                                    <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- #User Info -->
                    <!-- Menu -->
                    <div class="menu">
                        <ul class="list">
                            <li class="<?php if($this->uri->segment('2')=="Beranda"){ echo "active";}?>">
                                <a href="<?php echo base_url()?>admin/Beranda">
                                    <i class="material-icons">home</i>
                                    <span>Home</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment('2')=="Product"){ echo "active";}?>">
                                <a href="<?php echo base_url()?>admin/Product ">
                                    <i class="material-icons">assignment</i>
                                    <span>Product</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment('2')=="Pricelist"){ echo "active";}?>">
                                <a href="<?php echo base_url()?>admin/Pricelist ">
                                    <i class="material-icons">assignment</i>
                                    <span>Price List</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment('2')=="Berita"){ echo "active";}?>">
                                <a href="<?php echo base_url()?>admin/Berita">
                                    <i class="material-icons">comment</i>
                                    <span>News</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment('2')=="Slider"){ echo "active";}?>">
                                <a href="<?php echo base_url()?>admin/Slider">
                                    <i class="material-icons">images</i>
                                    <span style="margin-left: -5%">Slider</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment('2')=="Video"){ echo "active";}?>">
                                <a href="<?php echo base_url()?>admin/Video">
                                    <i class="material-icons">video_library</i>
                                    <span >Video</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment('2')=="Galery"){ echo "active";}?>">
                                <a href="<?php echo base_url()?>admin/Galery">
                                    <i class="material-icons">perm_media</i>
                                    <span >Galery</span>
                                </a>
                            </li>
                            <li class="header">LAINNYA</li>
                            <li>
                                <a  onclick="clik()" data-target="#defaultModal" type="button" href="javascript:void(0);">
                                    <i class="material-icons col-red">report</i>
                                    <span>About</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                    <!-- #Menu -->

                    <!-- Footer -->
                    <div class="legal">
                        <div class="copyright">
                            &copy; Developed by<a href="http://jemberkita.online"> Jemberkita</a>.
                        </div>
                        <div class="version">
                            <b>Jember </b> 2018
                        </div>
                    </div>
                    <!-- #Footer -->
                </aside>
    </section>
</body>

</html>
<script type="text/javascript">
    function clik(){
        swal({
        title: "SUZUKI JEMBER",
        text: "Web ini adalah admin untuk mengatur segala proses tampilan di web suzukijember.com \n Developed by jemberkita.online ",
        imageUrl: '<?php echo base_url() ?>master/about_logo.png'
        });
    }
    function logout(){
    var proc = window.confirm('Apakah Anda yakin akan menghapus data?');
    if(proc){
      document.location='<?php echo base_url("Home/logout"); ?>';
    }
}
</script>
