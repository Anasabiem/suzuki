<!-- Jquery Core Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url() ?>master/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="<?php echo base_url() ?>master/admin/plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="<?php echo base_url() ?>master/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/node-waves/waves.js"></script>

    <!-- ck editors -->
    <script type="text/javascript" src="<?php echo base_url() ?>master/ckeditor/ckeditor.js"></script>

 <!-- Light Gallery Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/light-gallery/js/lightgallery-all.js"></script>


    <!-- Jquery DataTable Plugin Js -->
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
        <script src="<?php echo base_url()?>master/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url()?>master/admin/js/admin.js"></script>
    <script src="<?php echo base_url()?>master/admin/js/pages/tables/jquery-datatable.js"></script>
    <script src="<?php echo base_url() ?>master/admin/js/pages/forms/form-wizard.js"></script>
    <script src="<?php echo base_url()?>master/admin/js/pages/ui/dialogs.js"></script>
   

    <!-- Custom Js -->
    <script src="<?php echo base_url()?>master/admin/js/pages/medias/image-gallery.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url()?>master/admin/js/demo.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url()?>master/admin/plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<?php echo base_url()?>master/admin/plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/flot-charts/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>master/admin/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url()?>master/admin/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>master/admin/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<?php echo base_url()?>master/admin/plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?php echo base_url()?>master/admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
   <!--  <script src="<?php echo base_url()?>master/admin/js/admin.js"></script> -->
    <script src="<?php echo base_url()?>master/admin/js/pages/index.js"></script>



    
    