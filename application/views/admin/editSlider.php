<?php $this->load->view('admin/side/head'); ?>
<?php $this->load->view('admin/side/navbar'); ?>
<?php $this->load->view('admin/side/js'); ?>
<section class="content">
  <!-- Default Example -->
  <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card">
              <div class="header">
                  <h2>
                      Gambar Slider
                  </h2>
                  <ul class="breadcrumb">
                      <li class="breadcrumb-item"><a href="<?php echo base_url("admin/slider") ?>">Slider</a></li>
                      <li class="breadcrumb-item active">Tambah product</li>
                  </ul>
              </div>
              <div class="body">
                  <div class="row">
                      <div class="col-xs-12 col-md-12">
                          <a href="javascript:void(0);" class="thumbnail">
                            <?php foreach ($gambar->result() as $g1) { ?>
                              <img src="<?php echo  base_url()?>gallery/gambar_slider/<?php echo $g1->gambar_slider; ?>" class="img-responsive">
                            <?php } ?>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- #END# Default Example -->
  <!-- Basic Validation -->
  <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card">
              <div class="header">
                  <h2>Edit Slider</h2>
              </div>
              <div class="body">
                  <?php echo form_open_multipart(base_url('admin/Slider/prosesEdit/'));?>
                      <div class="form-group form-float">
                          <div class="form-line">
                            <?php foreach ($gambar->result() as $judul) {?>
                              <input type="text" class="form-control" name="name" value="<?php echo $judul->judul_slider; ?>" required>
                              <input name="id_slider" value="<?php echo $judul->id_slider; ?>" hidden>
                            <?php } ?>
                              <label class="form-label">Judul</label>
                          </div>
                      </div>
                      <div class="form-group form-float">
                          <div class="form-line">
                            <?php foreach ($gambar->result() as $isi) { ?>
                              <textarea name="description" cols="30" rows="5" class="form-control no-resize" required><?php echo $isi->isi_slider; ?></textarea>
                            <?php } ?>
                              <label class="form-label">Deskripsi</label>
                          </div>
                      </div>
                      <div class="form-group form-float">
                          <div class="form-line">
                              <input class="btn btn-primary waves-effect" type="file" name="userfile" required>
                          </div>
                      </div>
                      <button class="btn btn-primary waves-effect" type="submit" value="upload">Simpan</button>
                      <a class="btn btn-primary waves-effect" href="<?php echo base_url(); ?>admin/Slider">Kembali</a>
                  </form>
                </div>
              </div>
          </div>
      </div>
  </div>
  <!-- #END# Basic Validation -->
</section>
