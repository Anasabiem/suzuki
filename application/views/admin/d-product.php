
<?php $this->load->view('admin/side/head'); ?>
<section class="content">
<div class="container-fluid">
<!-- Content -->
<!-- Input -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DETAIL DAN EDIT
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                  <div class="container-fluid">
                    <!-- Gambar -->
                    <div class="row clearfix">
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Gambar Mobil
                                </h2>
                            </div>
                            <div class="body align-center">
                              <?php foreach ($detail->result() as $key) { ?>
                                <img style="height:150px" src="<?php echo base_url()?>gallery/gambar_product/<?php echo $key->gambar_mobil; ?>" alt="Gambar Mobil">
                              <?php } ?>
                            </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Gambar Exterior
                                </h2>
                            </div>
                            <div class="body align-center">
                              <?php foreach ($detail->result() as $key) { ?>
                                <img style="height:150px" src="<?php echo base_url()?>gallery/gambar_product/<?php echo $key->gambar_exterior; ?>" alt="Gambar Exterior">
                              <?php } ?>
                            </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Gambar Interior
                                </h2>
                            </div>
                            <div class="body align-center">
                              <?php foreach ($detail->result() as $key) { ?>
                                <img style="height:150px" src="<?php echo base_url()?>gallery/gambar_product/<?php echo $key->gambar_interior; ?>" alt="Gambar Interior">
                              <?php } ?>
                            </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Gambar Performance
                                </h2>
                            </div>
                            <div class="body align-center">
                              <?php foreach ($detail->result() as $key) { ?>
                                <img style="height:150px" src="<?php echo base_url()?>gallery/gambar_product/<?php echo $key->gambar_performance; ?>" alt="Gambar Performance">
                              <?php } ?>
                            </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Gambar Safety
                                </h2>
                            </div>
                            <div class="body align-center">
                              <?php foreach ($detail->result() as $key) { ?>
                                <img style="height:150px" src="<?php echo base_url()?>gallery/gambar_product/<?php echo $key->gambar_safety; ?>" alt="Gambar Performance">
                              <?php } ?>
                            </div>
                        </div>
                      </div>
                    </div>
                    <!-- End of Gambar -->
                  </div>
                  <?php echo form_open_multipart(base_url('admin/Product/prosesEdit'));?>
                  <div class="col-sm-3">
                      <div class="form-group">
                        <?php foreach ($detail->result() as $key) { ?>
                          <?php $id=$key->id_mobil; ?>
                          <a type="button" class="col-sm-12 btn btn-primary waves-effect" href="<?php echo base_url('admin/Product/editGambarMobil/'.$id); ?>">Gambar Mobil</a>
                        <?php } ?>
                      </div>
                      <div class="form-group">
                            <a type="button" class="col-sm-12 btn btn-primary waves-effect" href="<?php echo base_url('admin/Product/editGambarExterior/'.$id); ?>"><span>Gambar Exterior</span></a>
                      </div>
                      <div class="form-group">
                            <a type="button" class="col-sm-12 btn btn-primary waves-effect" href="<?php echo base_url('admin/Product/editGambarInterior/'.$id); ?>">Gambar Interior</a>
                      </div>
                      <div class="form-group">
                            <a type="button" class="col-sm-12 btn btn-primary waves-effect" href="<?php echo base_url('admin/Product/editGambarPerformance/'.$id); ?>">Gambar Performance</a>
                      </div>
                      <div class="form-group">
                            <a type="button" class="col-sm-12 btn btn-primary waves-effect" href="<?php echo base_url('admin/Product/editGambarSafety/'.$id); ?>">Gambar Safety</a>
                      </div>
                  </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="form-line">
                                <small>Nama Produk Mobil</small>
                                <?php foreach ($detail->result() as $key) { ?>
                                    <input name="nama_mobil" type="text" class="form-control" placeholder="Nama Produk Mobil" value="<?php echo $key->nama_mobil; ?>" />
                                    <input name="id_mobil" value="<?php echo $key->id_mobil; ?>" hidden />
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <small>Type/Model</small>
                                  <select class="form-control show-tick" name="id_automobile" required>
                                      <option value="">---- Pilih Automobile ----</option>
                                      <?php
                                        foreach ($modelu as $s) {
                                      ?>
                                      <option value="<?php echo $s->id_automobile ?>"><?php echo $s->nama_automobile; ?></option>
                                      <?php
                                        }
                                      ?>
                                  </select>
                            </div>
                            <div class="">
                                <select class="form-control show-tick" name="id_model" required>
                                    <option value="">---- Pilih Type/Model ----</option>
                                    <?php
                                      foreach ($automobile as $a) {
                                    ?>
                                    <option value="<?php echo $a->id_model; ?>"><?php echo $a->nama_model; ?></option>
                                    <?php
                                      }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <small>Spesifikasi Panjang</small>
                                <?php foreach ($detail->result() as $key) { ?>
                                  <input name="spek_panjang" type="text" class="form-control" placeholder="Spesifikasi Panjang" value="<?php echo $key->spek_panjang; ?>" />
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <small>Spesifikasi Lebar</small>
                                <?php foreach ($detail->result() as $key) { ?>
                                  <input name="spek_lebar" type="text" class="form-control" placeholder="Spesifikasi Lebar" value="<?php echo $key->spek_lebar; ?>" />
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <small>Spesifikasi Tinggi</small>
                                <?php foreach ($detail->result() as $key) { ?>
                                  <input name="spek_tinggi" type="text" class="form-control" placeholder="Spesifikasi Tinggi" value="<?php echo $key->spek_tinggi; ?>" />
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <small>Spesifikasi Tempat Duduk</small>
                                <?php foreach ($detail->result() as $key) { ?>
                                  <input name="spek_tempat_duduk" type="text" class="form-control" placeholder="Spesifikasi Tempat Duduk" value="<?php echo $key->spek_tempat_duduk; ?>" />
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <small>Spesifikasi Tanki</small>
                                <?php foreach ($detail->result() as $key) { ?>
                                  <input name="spek_tanki" type="text" class="form-control" placeholder="pesifikasi Tanki" value="<?php echo $key->spek_tanki; ?>" />
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <small>Embed Code Video</small><br>
                                <?php foreach ($detail->result() as $key) { ?>
                                  <iframe width="300" src="<?php echo $key->embedcode_video_mobil; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                  <textarea name="embedcode_video_mobil" type="text" class="form-control" placeholder="Embed Code Video"><?php echo $key->embedcode_video_mobil; ?></textarea>
                                <?php } ?>
                            </div>
                            <br>
                            <p>Contoh embed code:</p>
                            <p>https://www.youtube.com/embed/44syBMDS2JA</p>
                        </div>
                        <i class="col-sm-4"></i>
                        <button class="col-sm-4 btn btn-primary" type="submit" title="Simpan" name="btnSimpan">Simpan</button>
                        <i class="col-sm-4"></i>
                    </div>
                  </form>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- #END# Input -->
<!-- End of Content -->
</div>
</section>
<?php $this->load->view('admin/side/navbar'); ?>
<?php $this->load->view('admin/side/js'); ?>
