<?php $this->load->view("admin/side/head"); ?>

<!-- Content -->
<section class="content">
  <div class="container-fluid">
    <!-- Input -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Tambah Daftar Harga
                    </h2>
                </div>
                <?php echo form_open_multipart(base_url('admin/Pricelist/insertPricelist'));?>
                <div class="body">
                    <div class="row clearfix">
                      <div class="col-sm-12">
                          <select class="form-control show-tick" name="modelu" required>
                              <option value="">-- Please select --</option>
                              <?php foreach ($pricelistTambah->result() as $key) { ?>
                              <option value="<?php echo $key->id_model; ?>"><?php echo $key->nama_model; ?></option>
                            <?php } ?>
                          </select>
                      </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Sales Model" name="sales_model" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Harga" name="harga" />
                                </div>
                            </div>
                            <button type="submit" class="btn bg-indigo waves-effect">SAVE CHANGES</button>
                        </div>
                    </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- #END# Input -->
    </div>
  </div>
</section>
<!-- Content -->

<?php $this->load->view('admin/side/navbar'); ?>
<?php $this->load->view('admin/side/js'); ?>
