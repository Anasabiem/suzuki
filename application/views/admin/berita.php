
<?php $this->load->view('admin/side/head'); ?>
<link href="<?php echo  base_url()?>master/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
              <div class="block-header">
                  <!-- <h2>
                      JQUERY DATATABLES
                  </h2> -->
              </div>
              <!-- Exportable Table -->
              <div class="row clearfix">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  SUZUKI BERITA
                              </h2>
                              
                          </div>
                          <div class="body">
                              <div class="table-responsive">
                                <div style="margin-bottom: 25px;">
                                  <a href="<?php echo base_url('admin/Berita/tambahData')?>">
                                  <button type="button" style="width: 30%;" class="btn btn-primary waves-effect">
                                      <i class="material-icons">add_to_queue</i>
                                      <span>TAMBAH BERITA</span>
                                  </button>
                                  </a>
                                </div>
                                  <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                      <thead>
                                          <tr>
                                              <th>No</th>
                                              <th>Judul Berita</th>
                                              <th>Tanggal Publish</th>
                                              <th>Sumber</th>
                                              <th>Gambar</th>
                                              <th>Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                            $n=1;
                                              foreach ($berita->result() as $i) {
                                        ?>
                                          <tr>
                                              <td><?php echo $n; ?></td>
                                              <td><?php echo $i-> judul_berita; ?></td>
                                              <td><?php echo date("d F Y", strtotime($i -> tgl_berita)) ?></td>
                                              <td><?php echo $i-> sumber; ?></td>
                                              <td><img src="<?php echo base_url().$i -> gambar_berita ?>" width="100" height="100"></td>
                                              <td style="text-align: center;" class="row clearfix js-sweetalert">
                                                <!-- <a href="#" class="btn btn-warning waves-effect" ><i class="material-icons">menu</i></a> -->
                                                  <a class="btn btn-danger waves-effect " data-type="confirm" onclick="deleted('<?php echo $i->id_berita; ?>')"><i class="material-icons">delete_sweep</i></a>
                                                </td>
                                          </tr>
                                          <?php $n++; } ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- #END# Exportable Table -->
    </div>
  </div>
</section>
<script type="text/javascript">
  function deleted(param){
    var proc = window.confirm('Apakah Anda yakin akan menghapus data?');
    if(proc){
      swal("Good job!", "You clicked the button!", "success");
      document.location='<?php echo base_url(); ?>admin/Berita/hapusdata/'+param;

    }
  }
  function deletede($id){
     swal({
        title: "Are you sure?",
        text: "Apakah Anda yakin ingin menghapus data ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Iya, hapus data!",
        closeOnConfirm: false
      },
      function(){
        if (swal()) {
        document.location='<?php echo base_url("admin/Berita/hapusdata/"); ?>';
        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
          }
        
        swal("Deleted!", "Your imaginary file has been deleted.", "success");
      });
  }
</script>
  <?php $this->load->view('admin/side/navbar'); ?>
  <?php $this->load->view('admin/side/js'); ?>